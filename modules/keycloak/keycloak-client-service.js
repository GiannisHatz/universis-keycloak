const unirest = require('unirest');
const {HttpError, DataError} = require('@themost/common');
const ApplicationService = require('@themost/common').ApplicationService;
const URL = require('url').URL;

/**
 * @typedef {object} KeycloakUserProfile
 * @property {string} sub (e.g. 00000000-0000-0000-0000-432730e4f6b2)
 * @property {string} name
 * @property {string} preferred_username
 * @property {string} given_name
 * @property {string} family_name
 * @property {string} email
 */

/**
 * @typedef {object} KeycloakServiceSettings
 * @property {string} server_uri
 * @property {string} client_id
 * @property {string} client_secret
 * @property {string} profile_uri
 * @property {string=} admin_uri
 * @property {{username: string,password:string, client_id?:string, client_secret:string, scope:string}} adminAccount
 */

/**
 * @typedef {object} KeycloakAuthorizeUser
 * @property {string} client_id
 * @property {string=} client_secret
 * @property {string} username
 * @property {string} password
 * @property {string} grant_type
 * @property {string=} scope
 */

/**
 * @typedef {object} KeycloakMethodOptions
 * @property {string} access_token
 */


/**
 * @class
 * @property {KeycloakServiceSettings} settings
 */
class KeycloakClientService extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        /**
         * @name KeycloakClientService#settings
         * @type {KeycloakServiceSettings}
         */
        Object.defineProperty(this, 'settings', {
            writable: false,
            value: app.getConfiguration().getSourceAt("settings/auth"),
            enumerable: false,
            configurable: false
        });
    }

    /**
     * Gets keycloak server root
     * @returns {string}
     */
    getServer() {
        return this.settings.server_uri;
    }

    /**
     * Gets keycloak server root
     * @returns {string}
     */
     getAdminRoot() {
        return this.settings.admin_uri;
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets user profile by calling keycloak server profile endpoint
     * @param {*} context
     * @param {string} token
     */
    getProfile(context, token) {
        if (this.settings.profile_uri == null) {
            return Promise.reject(new Error('An application configuration setting is missing. User profile URI is not defined or is inaccessible.'))
        }
        if (token == null) {
            return Promise.reject(new Error('User token cannot be empty at this context.'));
        }
        const self = this;
        return new Promise(function(resolve, reject) {
            return unirest.get(new URL('userinfo', self.getServer()))
                .header('Authorization', `Bearer ${token}`)
                .header('Accept', 'application/json')
                .end(function (response) {
                    const body =  response.body;
                    if (body && body.error) {
                        return reject(Object.assign(new HttpError(body.code || 500), body));
                    }
                    return resolve(body);
                });
        });
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets the token info of the current context
     * @param {*} context
     */
    getContextTokenInfo(context) {
        if (context.user == null) {
            return Promise.reject(new Error('Context user may not be null'));
        }
        if (context.user.authenticationType !== 'Bearer') {
            return Promise.reject(new Error('Invalid context authentication type'));
        }
        if (context.user.authenticationToken == null) {
            return Promise.reject(new Error('Context authentication data may not be null'));
        }
        return this.getTokenInfo(context, context.user.authenticationToken);
    }

    /**
     * Gets token info by calling keycloak server endpoint
     * @param {*} context
     * @param {string} token
     */
    getTokenInfo(context, token) {
        return new Promise((resolve, reject)=> {
            return unirest.post(new URL('token/introspect', this.getServer()))
                .header('Authorization', 'Basic ' + Buffer.from(this.settings.client_id + ':' + this.settings.client_secret).toString('base64'))
                .type('form')
                .send({
                    "token_type_hint":"access_token",
                    "token":token,
                }).end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    if (response.body && response.body.scope) {
                        // replace space with comma (for backward compatibility)
                        response.body.scope = response.body.scope.replace(/\s/g,',');
                    }
                    return resolve(response.body);
                });
        });
    }

    /**
     * @param {KeycloakAuthorizeUser} authorizeUser
     */
    authorize(authorizeUser) {
        return new Promise((resolve, reject)=> {
            return unirest.post(new URL('token', this.getServer()))
                .type('form')
                .send(authorizeUser).end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    /**
     * Gets a user by name
     * @param {*} user_id 
     * @param {KeycloakMethodOptions} options 
     */
     getKeycloakUserById(user_id, options) {
        return new Promise((resolve, reject) => {
            return unirest.get(new URL(`users/${user_id}`, this.getAdminRoot()))
                .header('Authorization', `Bearer ${options.access_token}`)
                .end((response) => {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    /**
     * Gets a user by name
     * @param {*} user_id 
     * @param {KeycloakMethodOptions} options 
     */
     async getUserById(user_id, options) {
        const result = await this.getKeycloakUserById(user_id, options);
        if (result) {
            return this.convertTo(result);
        }
    }

    /**
     * Gets a user by name
     * @param {string} username 
     * @param {KeycloakMethodOptions} options 
     */
     getKeycloakUser(username, options) {
        return new Promise((resolve, reject)=> {
            return unirest.get(new URL('users', this.getAdminRoot()))
                .header('Authorization', `Bearer ${options.access_token}`)
                .query({
                    username: username
                })
                .end((response) => {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    const result = response.body.find((item) => {
                        return item.username === username;
                    });
                    return resolve(result);
                });
        });
    }

    /**
     * Gets a user by name
     * @param {string} username 
     * @param {KeycloakMethodOptions} options 
     */
    async getUser(username, options) {
        const result = await this.getKeycloakUser(username, options);
        if (result) {
            return this.convertTo(result);
        }
    }

    /**
     * Gets a user by email address
     * @param {string} email 
     * @param {KeycloakMethodOptions} options 
     */
     getKeycloakUserByEmail(email, options) {
        return new Promise((resolve, reject)=> {
            return unirest.get(new URL('users', this.getAdminRoot()))
                .header('Authorization', `Bearer ${options.access_token}`)
                .query({
                    username: email
                })
                .end((response) => {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    const result = response.body.find((item) => {
                        return item.email === email;
                    });
                    return resolve(result);
                });
        });
    }

    /**
     * Gets a user by email address
     * @param {string} email 
     * @param {KeycloakMethodOptions} options 
     */
     async getUserByEmail(email, options) {
        const result = await this.getKeycloakUserByEmail(email, options);
        if (result) {
            return this.convertTo(result);
        }
    }

    /**
     * Updates an existing user
     * @param {GenericUser} user 
     * @param {KeycloakMethodOptions} options 
     */
     async updateUser(user, options) {
        // convert generic user
        const updateUser = this.convertFrom(user);
        // create keycloak user
        await this.updateKeycloakUser(updateUser, options);
        // finally convert new user to generic user
        return user;
    }

    /**
     * Updates an existing user
     * @param {KeycloakUser} user 
     * @param {KeycloakMethodOptions} options 
     */
     updateKeycloakUser(user, options) {
        return new Promise((resolve, reject)=> {
            if (user.id == null) {
                return reject(new DataError('E_IDENTIFIER', 'User may not be empty at this context.', null, 'User', 'id'));
            }
            const request = unirest.put(new URL(`users/${user.id}`, this.getAdminRoot()));
            return request.header('Authorization', `Bearer ${options.access_token}`)
                .header('Content-Type', `application/json`)
                .send(user)
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    /**
     * Converts a keycloak user to a generic user
     * @param {KeycloakUser|*} user
     */
    convertTo(user) {
        if (user) {
            return {
                id: user.id,
                name: user.username,
                alternateName: user.email,
                enabled: user.enabled,
                emailVerified: user.emailVerified,
                firstName: user.givenName,
                lastName: user.familyName
            }
        }
    }
    /**
     * Converts a generic user to a keycloak user presentation
     * @param {GenericUser|*} user 
     */
    convertFrom(user) {
        // convert user to keycloak user
        const result = {
            id: user.id,
            username: user.name,
            email: user.alternateName,
            enabled: user.enabled,
            emailVerified: Object.prototype.hasOwnProperty.call(user, 'emailVerified') ? user.emailVerified : true,
            firstName: user.givenName,
            lastName: user.familyName
         };
         if (user.userCredentials && user.userCredentials.userPassword) {
             let algorithm;
             let password;
             if (/^\{md5\}/ig.test(user.userCredentials.userPassword)) {
                throw new Error('Unsupported hash algorithm');
             } else if (/^\{clear\}/.test(user.userCredentials.userPassword)) {
                password = user.userCredentials.userPassword.replace(/^\{clear\}/ig, '');
            } else if (/^\{sha1\}/.test(user.userCredentials.userPassword)) {
                throw new Error('Unsupported hash algorithm');
            } else {
                password = user.userCredentials.userPassword;
            }
             Object.assign(result, {
                credentials: [
                    {
                        algorithm: algorithm,
                        temporary: Object.prototype.hasOwnProperty.call(user.userCredentials, 'temporary') ? user.userCredentials.temporary : false,
                        type: 'password',
                        value: password
                    }
                ]
             });
             if (result.credentials.temporary) {
                 Object.assign(result, {
                    requiredActions: [
                        'UPDATE_PASSWORD'
                    ]
                 });
             }
         }
         return result;
    }


    /**
     * Creates a new user
     * @param {KeycloakUser} user 
     * @param {KeycloakMethodOptions} options 
     */
     createKeycloakUser(user, options) {
        return new Promise((resolve, reject)=> {
            const request = unirest.post(new URL('users', this.getAdminRoot()));
            // convert generic user
            return request.header('Authorization', `Bearer ${options.access_token}`)
                .header('Content-Type', `application/json`)
                .send(user)
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    /**
     * Creates a new user
     * @param {GenericUser} user 
     * @param {KeycloakMethodOptions} options 
     */
     async createUser(user, options) {
        // convert generic user
        const newUser = this.convertFrom(user);
        // create keycloak user
        const result = await this.createKeycloakUser(newUser, options);
        // finally convert new user to generic user
        return newUser;
    }


    /**
     * Deletes a user
     * @param {{id: any}} user 
     * @param {KeycloakMethodOptions} options 
     */
     deleteUser(user, options) {
        return new Promise((resolve, reject)=> {
            if (user.id == null) {
                return reject(new DataError('E_IDENTIFIER', 'User may not be empty at this context.', null, 'User', 'id'));
            }
            const request = unirest.delete(new URL(`users/${user.id}`, this.getAdminRoot()));
            return request.header('Authorization', `Bearer ${options.access_token}`)
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

}

module.exports = {
    KeycloakClientService
};