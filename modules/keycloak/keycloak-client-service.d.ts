import { ApplicationService, ApplicationBase } from '@themost/common';
import { DataContext } from '@themost/data';

export declare interface KeycloakMethodOptions {
    access_token: string;
}

export declare interface KeycloakAuthorizeUser {
    client_id?: string;
    client_secret?: string;
    username: string;
    password: string;
    grant_type: string;
    scope?: string;
}

export declare interface KeycloakServiceSettings {
    unattendedExecutionAccount?: string;
    client_id: string;
    client_secret?: string;
    server_uri: string;
    profile_uri?: string;
    admin_uri?: string;
    adminAccount: {
        username: string;
        password: string;
        client_id: string;
        client_secret?: string;
        scope?: string;
    }
}

export declare interface KeycloakUserProfile {
    sub: string;
    name: string;
    preferred_username: string;
    given_name: string;
    family_name: string;
    email: string;
}

export declare interface GenericUser {
     public id?: any; 
     public additionalType?: string; 
     public alternateName?: string; 
     public description?: string; 
     public givenName?: string; 
     public familyName?: string; 
     public image?: string; 
     public name?: string; 
     public url?: string; 
     public dateCreated?: Date; 
     public dateModified?: Date; 
     public createdBy?: any; 
     public modifiedBy?: any; 
     public lockoutTime?: Date; 
     public logonCount?: number; 
     public enabled?: boolean; 
     public lastLogon?: Date; 
     public userCredentials?: {
         userPassword?: string;
         userActivated?: boolean;
         temporary?: boolean;
     }
}

export declare interface KeycloakUser {
    public id?: any; 
    public username?: string; 
    public email?: string; 
    public enabled?: boolean;
    public emailVerified?: boolean;
    public firstName?: string; 
    public lastName?: string; 
    public credentials?: {
        algorithm?: string,
        temporary?: boolean,
        type?: string,
        value?: string
    }
}


export declare class KeycloakClientService extends ApplicationService {
    get settings(): KeycloakServiceSettings;
    constructor(app: ApplicationBase)
    getProfile(context: DataContext, token: string): Promise<KeycloakUserProfile>;
    getTokenInfo(context: DataContext, token: string): Promise<any>;
    getContextTokenInfo(context: DataContext): Promise<any>;
    authorize(authorizeUser: KeycloakAuthorizeUser): Promise<{ access_token?: string, refresh_token?: string}>;
    getUser(username: string, options: KeycloakMethodOptions): Promise<any>;
    getUserById(user_id: any, options: KeycloakMethodOptions): Promise<any>;
    getUserByEmail(email: string, options: KeycloakMethodOptions): Promise<any>;
    updateUser(user: GenericUser | any, options: KeycloakMethodOptions): Promise<any>;
    createUser(user: GenericUser | any, options: KeycloakMethodOptions): Promise<any>;
    deleteUser(user: { id: any }, options: KeycloakMethodOptions): Promise<any>; 
}