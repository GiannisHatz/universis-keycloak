const {DataApplication} = require('@themost/data');
const {KeycloakClientService} = require('../modules/keycloak');
const path = require('path');
const { TextUtils } = require('@themost/common');

describe('KeycloakClientService', () => {
    let app;
    beforeAll(() => {
        app = new DataApplication(path.resolve(__dirname));
        app.configuration.setSourceAt("settings/auth", {
            "unattendedExecutionAccount": "2S4TgUW6==",
            "server_uri": "http://localhost:8080/auth/realms/master/protocol/openid-connect/",
            "admin_uri": "http://localhost:8080/auth/admin/realms/master/",
            "client_id": "any-client",
            "adminAccount": {
                "username": "admin",
                "password": "secret"
            }
        });
    });
    it('should create service', () => {
        const service = new KeycloakClientService(app);
        expect(service).toBeTruthy();   
    });
    it('should should authorize user', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        expect(token).toBeTruthy();
        expect(token.access_token).toBeTruthy();
        expect(token.expires_in).toBeTruthy();
        await expectAsync(service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret1'
        })).toBeRejected();
    });

    it('should should authorize admin account', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: service.settings.adminAccount.username,
            password: service.settings.adminAccount.password
        });
        expect(token).toBeTruthy();
    });

    it('should should get user by name', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        const user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user).toBeTruthy();
        expect(user.name).toBe('admin');
    });

    it('should should get user by id', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        let user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user).toBeTruthy();
        user = await service.getUserById(user.id, {
            access_token: token.access_token
        });
        expect(user.name).toBe('admin');
    });

    it('should should get user by email', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        const user = await service.getUserByEmail('admin@example.com', {
            access_token: token.access_token
        });
        expect(user).toBeFalsy();
    });

    it('should should update user', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        let user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user).toBeTruthy();
        await service.updateUser({
            id: user.id,
            alternateName: 'admin1@example.com',
            emailVerified: true
        }, {
            access_token: token.access_token
        });
        user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user.alternateName).toBe('admin1@example.com');
    });

    it('should should create user', async () => {
        const service = new KeycloakClientService(app);
        const token = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'admin',
            password: 'secret'
        });
        let user = await service.getUser('admin', {
            access_token: token.access_token
        });
        expect(user).toBeTruthy();
        await service.createUser({
            name: 'alexis.rees@example.com',
            alternateName: 'alexis.rees@example.com',
            enabled: true,
            emailVerified: true,
            firstName: 'Alexis',
            lastName: 'Rees',
            userCredentials: {
                userPassword: `{clear}testSecret`
            }
        },{
            access_token: token.access_token
        });
        user = await service.getUser('alexis.rees@example.com', {
            access_token: token.access_token
        });
        expect(user.name).toBe('alexis.rees@example.com');

        //try to login
        const userToken = await service.authorize({
            client_id: 'admin-cli',
            grant_type: 'password',
            username: 'alexis.rees@example.com',
            password: 'testSecret'
        });
        expect(userToken).toBeTruthy();
        expect(userToken.access_token).toBeTruthy();

        await service.deleteUser({
            id: user.id
        },{
            access_token: token.access_token
        });
        user = await service.getUser('alexis.rees@example.com', {
            access_token: token.access_token
        });
        expect(user).toBeFalsy();

    });

});